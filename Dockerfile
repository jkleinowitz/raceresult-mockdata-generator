FROM rust:1.78 as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
#RUN apt-get update && apt-get install -y extra-runtime-dependencies && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/rr_gen /usr/local/bin/myapp
COPY --from=builder /usr/src/myapp/config/default.toml /etc/myapp/default.toml
CMD ["myapp"]