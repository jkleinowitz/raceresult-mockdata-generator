# RaceResult Mockdata Generator

This is a simple mockdata generator for the RaceResult API. It generates a random number of athletes and time entries 
for https://www.raceresult.com/de-at/home/index.php. Right now most values are hardcoded and require code changes.

## Installation

```bash
cargo install --path .

# BUild release version
cargo build --release
# Dockerfile
docker build -t rr_gen .
```