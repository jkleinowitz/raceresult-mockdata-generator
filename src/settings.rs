use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub num_runners: i64,
    pub tick_rate_secs: i64,
    pub runner_base_speed_m_s: f64,
    pub initial_ticks: i64,
    pub bind_addr: String,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let s = Config::builder()
            .add_source(File::with_name("/etc/myapp/default").required(false))
            .add_source(File::with_name("config/default").required(false))
            .add_source(Environment::with_prefix("RR_GEN"))
            .build()?;

        s.try_deserialize()
    }
}