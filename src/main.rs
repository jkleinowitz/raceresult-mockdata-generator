use std::collections::{BTreeMap, HashMap};
use std::fmt;
use std::sync::{Arc, RwLock};

use axum::{Json, Router, routing::get};
use axum::body::Body;
use axum::extract::{Query, State};
use axum::http::{Response, StatusCode};
use axum::response::Html;
use axum_macros::debug_handler;
use chrono::{DateTime, Duration, Utc};
use chrono::serde::ts_seconds_option;
use itertools::Itertools;
use lazy_static::lazy_static;
use minijinja::{context, Environment};
use rand::distributions::{Distribution, Standard};
use rand::Rng;
use rand::seq::IndexedRandom;
use serde::{Deserialize, Serialize};
use serde_with::{DurationSeconds, serde_as};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

mod settings;

#[serde_as]
#[derive(Default, Serialize, Deserialize, Debug)]
struct AppState {
    runners: Vec<Runner>,
    start_time: DateTime<Utc>,
    #[serde_as(as = "DurationSeconds<i64>")]
    tick_rate: Duration,
    stages_for_comp: HashMap<Competition, Vec<Stage>>,
    distances_for_comp: HashMap<Competition, Vec<i64>>,

    ranks_for_comp: HashMap<CompetitonGender, Vec<i64>>,
    current_time: DateTime<Utc>,
    distance_traveled_for_runner: HashMap<i64, f64>,
    speed_per_runner: HashMap<i64, f64>,
    stages_cleared_for_runner: HashMap<i64, Vec<StageTime>>,
}


impl AppState {
    fn new() -> AppState {
        let mut app_state = AppState::default();

        app_state.runners = (0..CONFIG.num_runners)
            .map(|i| Runner::random_runner(i + 1))
            .collect::<Vec<_>>();

        app_state.runners.iter().group_by(|r| r.comp)
            .into_iter().for_each(|(comp, runners)| {
            runners.group_by(|x| x.gender).into_iter().for_each(|(gender, gendered_runners)| {
                let runners = gendered_runners.map(|r| r.st_nr).collect::<Vec<_>>();
                let comp_gen = CompetitonGender {
                    comp,
                    gender,
                };
                app_state.ranks_for_comp.insert(comp_gen, runners);
            });
        });

        app_state.runners.iter().for_each(|r| {
            app_state.distance_traveled_for_runner.insert(r.st_nr, 0.0);
        });

        app_state.start_time = Utc::now();
        app_state.current_time = app_state.start_time;
        app_state.tick_rate = Duration::seconds(CONFIG.tick_rate_secs);
        app_state.stages_for_comp.insert(Competition::_14K, vec![
            Stage::Start,
            Stage::Koenigstal,
            Stage::Hohemutalm,
            Stage::Koenigstal2,
            Stage::Ziel,
        ]);

        app_state.stages_for_comp.insert(Competition::_26K, vec![
            Stage::Start,
            Stage::Koenigstal,
            Stage::Timmelstal,
            Stage::Lenzenalm,
            Stage::Kueppelehuette,
            Stage::Ramolhaus,
            Stage::Piccardbruecke,
            Stage::Hohemutalm,
            Stage::Koenigstal2,
            Stage::Ziel,
        ]);

        app_state.stages_for_comp.insert(Competition::_42K, vec![
            Stage::Start,
            Stage::Koenigstal,
            Stage::Timmelstal,
            Stage::Lenzenalm,
            Stage::Kueppelehuette,
            Stage::Ramolhaus,
            Stage::Piccardbruecke,
            Stage::Langtalereckhuette,
            Stage::Speicherseerotmoos,
            Stage::Wegweisermutruecken,
            Stage::Ziel,
        ]);

        app_state.stages_for_comp.insert(Competition::_62K, vec![
            Stage::Start,
            Stage::Koenigstal,
            Stage::Timmelstal,
            Stage::Lenzenalm,
            Stage::Kueppelehuette,
            Stage::Ramolhaus,
            Stage::Piccardbruecke,
            Stage::Langtalereckhuette,
            Stage::Speicherseerotmoos,
            Stage::Wegweisermutruecken,
            Stage::Hohemutalm,
            Stage::Koenigstal2,
            Stage::Ziel,
        ]);

        Competition::iter().for_each(|comp| {
            if app_state.stages_for_comp.contains_key(&comp) {
                app_state.distances_for_comp.insert(comp, app_state.stages_for_comp.get(&comp).unwrap().iter().enumerate().map(|(i, _)| {
                    1000 * i as i64
                }).collect::<Vec<_>>());
            }
        });

        app_state.speed_per_runner = app_state.runners.iter().map(|r| (r.st_nr, r.speed_modifier)).collect::<HashMap<_, _>>();
        app_state
    }

    fn advance(&mut self) {
        self.advance_time();
        self.advance_runners();
        self.update_stages_cleared_for_runner();
        self.update_ranks_for_comp();
    }
    fn advance_time(&mut self) {
        self.current_time = self.current_time + self.tick_rate;
    }
    fn advance_runners(&mut self) {
        let tick_rate_seconds = self.tick_rate.num_seconds() as f64;
        self.distance_traveled_for_runner.iter_mut().for_each(|(st_nr, distance)| {
            *distance += CONFIG.runner_base_speed_m_s * tick_rate_seconds * self.speed_per_runner.get(st_nr).unwrap();
        });
    }

    fn update_ranks_for_comp(&mut self) {
        self.runners.iter()
            .sorted_by_key(|r| r.comp)
            .group_by(|r| r.comp)
            .into_iter().for_each(|(comp, runners)| {
            runners.sorted_by_key(|r| r.gender).group_by(|x| x.gender).into_iter().for_each(|(gender, gendered_runners)| {
                let mut ranks = gendered_runners.map(|r| r.st_nr).collect::<Vec<_>>();
                tracing::debug!("comp: {:?}, gender: {:?}, ranks: {:?}", comp, gender, ranks);
                ranks.sort_by(|a, b| {
                    self.distance_traveled_for_runner.get(b).unwrap().partial_cmp(self.distance_traveled_for_runner.get(a).unwrap()).unwrap()
                });
                self.ranks_for_comp.insert(CompetitonGender { comp, gender }, ranks);
            });
        });
    }

    fn update_stages_cleared_for_runner(&mut self) {
        let mut stages_cleared_for_runner: HashMap<i64, Vec<StageTime>> = self.stages_cleared_for_runner.clone();
        self.distance_traveled_for_runner.iter().for_each(|(st_nr, distance)| {
            let stages = self.stages_for_comp.get(&self.runners.iter().find(|r| r.st_nr == *st_nr).unwrap().comp).unwrap();
            let distances = self.distances_for_comp.get(&self.runners.iter().find(|r| r.st_nr == *st_nr).unwrap().comp).unwrap();
            let mut stage_times: Vec<StageTime> = stages_cleared_for_runner.get(st_nr).unwrap_or(&vec![]).clone();
            let stages_cleared = stage_times.iter().map(|st| st.stage.clone()).collect::<Vec<_>>();
            for (stage, stage_distance) in stages.iter().zip(distances.iter()) {
                if !stages_cleared.contains(stage) && *distance as i64 >= *stage_distance {
                    stage_times.push({
                        StageTime {
                            stage: stage.clone(),
                            time: self.current_time,
                        }
                    });
                }
            }
            stages_cleared_for_runner.insert(*st_nr, stage_times);
        });
        self.stages_cleared_for_runner = stages_cleared_for_runner;
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct TimeEntriesParams {
    should_update: bool,

    #[serde(with = "ts_seconds_option")]
    advance_to: Option<DateTime<Utc>>,
}

impl Default for TimeEntriesParams {
    fn default() -> Self {
        TimeEntriesParams {
            should_update: true,
            advance_to: None,
        }
    }
}

lazy_static! {
    static ref CONFIG: settings::Settings =
        settings::Settings::new().expect("config can be loaded");
}

lazy_static! {
    static ref TEMPLATES: Environment<'static> = {
        let mut env = Environment::new();
        env.add_template("layout", include_str!("../templates/layout.jinja")).unwrap();
        env.add_template("index", include_str!("../templates/index.jinja")).unwrap();
        env
    };
}

type SharedState = Arc<RwLock<AppState>>;

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "rr_gen=info,tower_http=debug,axum::rejection=trace".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let shared_state = SharedState::new(RwLock::new(AppState::new()));
    {
        if CONFIG.initial_ticks > 0 {
            tracing::info!("Initializing state to {} ticks", CONFIG.initial_ticks );
            let mut state = shared_state.write().unwrap();
            for _ in 0..CONFIG.initial_ticks {
                state.advance();
            }
        } else {
            tracing::info!("Initializing state at 0 ticks" );
        }
    }

    // build our application with a route
    let app = Router::new()
        .route("/", get(handler_home))
        .route("/time_entries", get(time_entries))
        .route("/current_state", get(current_state))
        .route("/reset", get(reset))
        .with_state(Arc::clone(&shared_state));

    // run it
    let listener = tokio::net::TcpListener::bind(CONFIG.bind_addr.clone())
        .await
        .unwrap();
    tracing::info!("listening on {}", listener.local_addr().unwrap());
    axum::serve(listener, app).await.unwrap();
}

/*
#[derive(Serialize, Deserialize)]
struct RaceResultResponse {
    time_entries: Vec<ResultLine>,
}*/
type RaceResultResponse = Vec<ResultLine>;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Hash, Copy, Clone, Ord, PartialOrd, EnumIter)]
enum Competition {
    _200,
    _400,
    _600,
    _1500,
    TMR,
    _14K,
    _26K,
    _42K,
    _62K,
}

impl fmt::Display for Competition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Competition::_42K => write!(f, "42K"),
            Competition::_62K => write!(f, "62K"),
            Competition::_200 => write!(f, "200m"),
            Competition::_400 => write!(f, "400m"),
            Competition::_600 => write!(f, "600m"),
            Competition::_1500 => write!(f, "1500m"),
            Competition::TMR => write!(f, "TMR"),
            Competition::_14K => write!(f, "14K"),
            Competition::_26K => write!(f, "26K"),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash, Serialize, Deserialize)]
struct CompetitonGender {
    comp: Competition,
    gender: Gender,
}

impl From<&Runner> for CompetitonGender {
    fn from(runner: &Runner) -> Self {
        CompetitonGender {
            comp: runner.comp,
            gender: runner.gender,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash, Ord, PartialOrd, Deserialize, Serialize)]
enum Gender {
    M,
    W,
}

impl Distribution<Gender> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Gender {
        match rng.gen_range(0..=1) { // rand 0.8
            0 => Gender::M,
            1 => Gender::W,
            _ => panic!("Doesnt exist")
        }
    }
}

impl Distribution<Competition> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Competition {
        match rng.gen_range(0..=3) { // rand 0.8
            0 => Competition::_14K,
            1 => Competition::_26K,
            2 => Competition::_42K,
            3 => Competition::_62K,
            _ => panic!("Doesnt exist")
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Runner {
    comp: Competition,
    gender: Gender,
    st_nr: i64,
    last_name: String,
    first_name: String,
    jg: i64,
    nation: String,
    club: String,
    ak: String,
    speed_modifier: f64,
}

impl Runner {
    fn random_runner(st_nr: i64) -> Runner {
        let female_first_names = vec!["Anna", "Maria", "Eva", "Hannah", "Sophie", "Lena", "Laura", "Lea", "Sarah", "Julia"];
        let male_first_names = vec!["Max", "Moritz", "Erik", "Lukas", "Simon", "Jonas", "David", "Paul", "Jakob", "Felix"];
        let last_names = vec!["Mustermann", "Musterfrau", "Muster", "Müller", "Schmidt", "Schneider", "Fischer", "Weber", "Meyer", "Wagner"];
        let clubs = vec!["SVL", "SVÖ", "SVS", "SVK", "SVW", "SVT", "SVR", "SVG", "SVB", "SVH"];

        let mut rng = rand::thread_rng();

        let gender: Gender = rng.gen();
        let first_names = match gender {
            Gender::M => male_first_names,
            Gender::W => female_first_names
        };
        let ak = match gender {
            Gender::M => "Master M".to_string(),
            Gender::W => "Master W".to_string()
        };

        Runner {
            comp: rng.gen(),
            gender,
            first_name: first_names.choose(&mut rng).unwrap().to_string(),
            last_name: last_names.choose(&mut rng).unwrap().to_string(),
            st_nr,
            jg: rng.gen_range(1970..2012),
            nation: "AUT".to_string(),
            club: clubs.choose(&mut rng).unwrap().to_string(),
            ak,
            speed_modifier: rng.gen_range(0.8..1.2),
        }
    }
}

#[derive(Eq, Hash, Debug, Clone, PartialEq, Serialize, Deserialize, EnumIter, Ord, PartialOrd)]
enum Stage {
    Start,
    Koenigstal,
    Timmelstal,
    Lenzenalm,
    Kueppelehuette,
    Ramolhaus,
    Piccardbruecke,
    Langtalereckhuette,
    Speicherseerotmoos,
    Wegweisermutruecken,
    Hohemutalm,
    Koenigstal2,
    Ziel,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ResultLine {
    #[serde(rename = "StNr.")]
    pub st_nr: i64,
    #[serde(rename = "Bewerb")]
    pub bewerb: String,
    #[serde(rename = "Platz")]
    pub platz: String,
    #[serde(rename = "Nachname")]
    pub nachname: String,
    #[serde(rename = "Vorname")]
    pub vorname: String,
    #[serde(rename = "Jg.")]
    pub jg: i64,
    pub mw: String,
    #[serde(rename = "Nation")]
    pub nation: String,
    #[serde(rename = "Verein")]
    pub verein: String,
    #[serde(rename = "AK")]
    pub ak: String,

    #[serde(flatten)]
    time_entries: BTreeMap<Stage, String>,

    #[serde(rename = "Diff.")]
    pub diff: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
struct StageTime {
    stage: Stage,
    time: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct JsonState {
    runners: Vec<Runner>,
    start_time: DateTime<Utc>,
    stages_for_comp: HashMap<Competition, Vec<Stage>>,
    distances_for_comp: HashMap<Competition, Vec<i64>>,
    //
    //ranks_for_comp: HashMap<CompetitonGender, Vec<i64>>,
    current_time: DateTime<Utc>,
    distance_traveled_for_runner: HashMap<i64, f64>,
    speed_per_runner: HashMap<i64, f64>,
    stages_cleared_for_runner: HashMap<i64, Vec<StageTime>>,

    progress_per_runner: HashMap<String, f64>,
    progress_per_competition: HashMap<String, f64>,
}

#[debug_handler]
async fn current_state(State(state): State<SharedState>) -> Json<JsonState> {
    let state = &state.read().unwrap();
    let mut progress_per_runner = HashMap::new();
    for r in state.runners.iter() {
        let distance = state.stages_cleared_for_runner.get(&r.st_nr).map(|v| v.len()).unwrap_or(0) as f64;
        let max_distance = state.distances_for_comp.get(&r.comp).map(|v| v.len()).expect("comp not found");
        progress_per_runner.insert(format!("{} {}", r.first_name, r.last_name), distance / max_distance as f64);
    }

    let mut progress_per_competition = HashMap::new();

    let copy = JsonState {
        runners: state.runners.clone(),
        start_time: state.start_time,
        //tick_rate: state.tick_rate,
        stages_for_comp: state.stages_for_comp.clone(),
        distances_for_comp: state.distances_for_comp.clone(),
        //ranks_for_comp: state.ranks_for_comp.clone(),
        current_time: state.current_time,
        distance_traveled_for_runner: state.distance_traveled_for_runner.clone(),
        speed_per_runner: state.speed_per_runner.clone(),
        stages_cleared_for_runner: state.stages_cleared_for_runner.clone(),
        progress_per_runner,
        progress_per_competition,
    };

    Json(copy)
}

async fn handler_home() -> Result<Html<String>, StatusCode> {
    let template = TEMPLATES.get_template("index").unwrap();

    let rendered = template
        .render(context! {
            title => "Home",
            welcome_text => "Hello World!",
        })
        .unwrap();

    Ok(Html(rendered))
}

async fn reset(State(state): State<SharedState>) -> Response<Body> {
    tracing::info!("Resetting current state");

    let mut state = state.write().unwrap();
    *state = AppState::new();

    Response::builder()
        .status(StatusCode::OK)
        .body(Body::from("ok"))
        .unwrap()
}

#[debug_handler]
async fn time_entries(params: Option<Query<TimeEntriesParams>>, State(state): State<SharedState>) -> Json<RaceResultResponse> {
    tracing::info!("time_entries, params: {:?}", params);

    {
        if let Some(actual_params) = params {
            if let Some(advance_to) = actual_params.advance_to {
                let mut state = state.write().expect("mutex was poisoned");
                let delta = advance_to - state.current_time;
                let needed_ticks = delta.num_seconds() / state.tick_rate.num_seconds();

                tracing::info!("advance_to: {:?}, delta: {:?}, needed_ticks: {:?}", advance_to, delta, needed_ticks);
                for _ in 0..needed_ticks {
                    state.advance();
                }
            }

            let mut state = state.write().expect("mutex was poisoned");
            if actual_params.should_update {
                state.advance();
            }
        } else {
            let mut state = state.write().expect("mutex was poisoned");
            state.advance();
        }
    }
    let state = &state.read().unwrap();

    tracing::debug!("ranks_for_comp: {:?}", state.ranks_for_comp);

    let all_entries = state.runners.iter().map(|runner| {
        let mut time_entries = BTreeMap::new();
        let mut time_for_last_stage = None;
        if let Some(stage_times) = state.stages_cleared_for_runner.get(&runner.st_nr) {
            stage_times.iter().for_each(|st| {
                time_entries.insert(st.stage.clone(), st.time.format("%H:%M:%S").to_string());
            });
            time_for_last_stage = stage_times.last();
        }


        let time_for_first = state.ranks_for_comp.get(&runner.into())
            .map(|x| x.first()).flatten()
            .map(|id| state.stages_cleared_for_runner.get(id))
            .flatten()
            .map(|x1| x1.iter()
                .find_or_last(|x| {
                    time_for_last_stage.map(|x2| x2.stage == x.stage).unwrap_or(false)
                }))
            .flatten();


        let diff = match (time_for_first, time_for_last_stage) {
            (Some(time_for_first), Some(time_for_last_stage)) => {
                let delta = if time_for_first.stage == time_for_last_stage.stage {
                    time_for_last_stage.time - time_for_first.time
                } else {
                    time_for_first.time - time_for_last_stage.time
                };

                if delta.num_seconds() > 0 {
                    let seconds = delta.num_seconds() % 60;
                    let minutes = (delta.num_seconds() / 60) % 60;
                    let hours = (delta.num_seconds() / 60) / 60;

                    format!("+{:0>2}:{:0>2}:{:0>2}", hours, minutes, seconds)
                } else if delta.num_seconds() == 0 {
                    "--".to_string()
                } else {
                    "".to_string()
                }
            }
            (_, _) => "".to_string()
        };


        for stage in Stage::iter().filter(|s| !time_entries.contains_key(s)).sorted() {
            time_entries.insert(stage.clone(), "".to_string());
        }
        let rank = state.ranks_for_comp.get(&runner.into()).unwrap().iter().position(|&r| r == runner.st_nr);
        let rank = match rank {
            Some(rank) => (rank + 1).to_string() + ".",
            None => "".to_string(),
        };


        ResultLine {
            st_nr: runner.st_nr,
            bewerb: runner.comp.to_string(),
            platz: rank,
            nachname: runner.last_name.clone(),
            vorname: runner.first_name.clone(),
            jg: runner.jg,
            mw: format!("{:?}", runner.gender).to_lowercase(),
            nation: runner.nation.clone(),
            verein: runner.club.clone(),
            ak: runner.ak.clone(),
            time_entries,
            diff,
        }
    }).collect::<Vec<_>>();
//tracing::debug!("distance_traveled_for_runner: {:?}", state.distance_traveled_for_runner);
    Json(all_entries)
}